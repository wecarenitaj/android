
package com.wecare.android.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import com.wecare.android.ui.base.BaseTabsViewPagerAdapter;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;

@Module(includes = MainActivityModule.BindsModule.class)
public class MainActivityModule {

    @Provides
    BaseTabsViewPagerAdapter provideFeedPagerAdapter(MainActivity activity) {
        return new BaseTabsViewPagerAdapter(activity, activity.getSupportFragmentManager());
    }

    @Module
    public interface BindsModule {
        @Binds
        abstract AppCompatActivity bindActivity(MainActivity mainActivity);
    }

}
