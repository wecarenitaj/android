package com.wecare.android.ui.main.profile.userProfile.schduler;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wecare.android.BR;
import com.wecare.android.R;
import com.wecare.android.ViewModelProviderFactory;
import com.wecare.android.data.model.api.models.ScheduleDayModel;
import com.wecare.android.data.model.api.responses.UserScheduleResponse;
import com.wecare.android.databinding.ActivityServicesSchedulerBinding;
import com.wecare.android.ui.base.BaseActivity;
import com.wecare.android.utils.AppConstants;
import com.wecare.android.utils.DateUtils;
import com.wecare.android.utils.DialogFactory;
import com.wecare.android.utils.WeCareUtils;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatButton;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

public class CaregiverServicesSchedulerActivity extends BaseActivity<ActivityServicesSchedulerBinding, CaregiverServicesSchedulerViewModel> implements CaregiverServicesSchedulerNavigator {

    @Inject
    ViewModelProviderFactory factory;
    private CaregiverServicesSchedulerViewModel viewModel;

    ArrayList<ScheduleDayModel> selectedDays = new ArrayList<ScheduleDayModel>(7);

    ScheduleDayModel sameDayModel = new ScheduleDayModel(0);

    ActivityServicesSchedulerBinding binding;

    //by default
    boolean isSameTimes = true;
    boolean is24Available;

    @Override
    public CaregiverServicesSchedulerViewModel getViewModel() {
        viewModel = ViewModelProviders.of(this, (ViewModelProvider.Factory) factory).get(CaregiverServicesSchedulerViewModel.class);
        return viewModel;
    }

    @Override
    public int getBindingVariable() {
        return BR.viewModel;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_services_scheduler;
    }

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, CaregiverServicesSchedulerActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel.setNavigator(this);
        binding = getViewDataBinding();
        addToolbar(R.id.toolbar, getString(R.string.schedule_services), true);
        viewModel.getUserSchedule();
        binding.fullTimeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                handle24AvailabilityUI(b);
            }
        });
    }

    private void handle24AvailabilityUI(boolean is24Available) {
        if (is24Available) {
            binding.sameTimeSelectionLayout.setVisibility(View.GONE);
            binding.allDaysScrollView.setVisibility(View.VISIBLE);
            binding.scheduledDaysScrollView.setVisibility(View.GONE);
            this.is24Available = true;
        } else {
            binding.sameTimeSelectionLayout.setVisibility(View.VISIBLE);
            binding.allDaysScrollView.setVisibility(View.GONE);
            binding.scheduledDaysScrollView.setVisibility(View.VISIBLE);
            this.is24Available = false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.saveItem:
                if (is24Available) {
                    viewModel.createSchedule(selectedDays, is24Available);
                    return true;
                }
                if (selectedDays.size() > 0) {
                    if (validateTimeSelection())
                        viewModel.createSchedule(selectedDays, is24Available);
                } else
                    DialogFactory.createErrorDialog(this, "", getString(R.string.error_select_days_times));
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return true;
    }

    private boolean validateTimeSelection() {
        for (ScheduleDayModel model : selectedDays) {
            if (model.getStartTime() == null || model.getStartTime().isEmpty()) {
                DialogFactory.createErrorDialog(this, "", getString(R.string.error_select_start_time) + " " + WeCareUtils.getDayNameByIndex(this, model.getCurrentDayNumber()));
                return false;
            } else if (model.getEndTime() == null || model.getEndTime().isEmpty()) {
                DialogFactory.createErrorDialog(this, "", getString(R.string.error_select_end_time) + " " + WeCareUtils.getDayNameByIndex(this, model.getCurrentDayNumber()));
                return false;
            }
        }
        return true;
    }

    @Override
    public void daySelected(int dayIndex, boolean isSelected, boolean isUserSelection) {

        if (isUserSelection) {
            //add day object in case selected from UI
            if (isSelected)
                selectedDays.add(new ScheduleDayModel(dayIndex));
            else
                selectedDays.remove(getSelectedDayByIndex(dayIndex));
        }

        switch (dayIndex) {
            case AppConstants.SUNDAY_INDEX:
                handleDayWeekButtonSelection(binding.sundayBtn, binding.sundayLayout, dayIndex, isSelected);
                viewModel.getSundayChecked().set(isSelected);
                break;
            case AppConstants.MONDAY_INDEX:
                handleDayWeekButtonSelection(binding.monBtn, binding.mondayLayout, dayIndex, isSelected);
                viewModel.getMondayChecked().set(isSelected);
                break;
            case AppConstants.TUESDAY_INDEX:
                handleDayWeekButtonSelection(binding.tuesdayBtn, binding.tuesdayLayout, dayIndex, isSelected);
                viewModel.getTuesdayChecked().set(isSelected);
                break;
            case AppConstants.WEDNSDAY_INDEX:
                handleDayWeekButtonSelection(binding.wendsdayBtn, binding.wednesdayLayout, dayIndex, isSelected);
                viewModel.getWednesdayChecked().set(isSelected);
                break;
            case AppConstants.THURSDAY_INDEX:
                handleDayWeekButtonSelection(binding.thursdayBtn, binding.thursdayLayout, dayIndex, isSelected);
                viewModel.getThursdayChecked().set(isSelected);
                break;
            case AppConstants.FRIDAY_INDEX:
                handleDayWeekButtonSelection(binding.fridayBtn, binding.fridayLayout, dayIndex, isSelected);
                viewModel.getFridayChecked().set(isSelected);
                break;
            case AppConstants.SATURDAY_INDEX:
                handleDayWeekButtonSelection(binding.saturdayBtn, binding.saturdayLayout, dayIndex, isSelected);
                viewModel.getSaturdayChecked().set(isSelected);
                break;
        }
    }

    @Override
    public void timesOfCaresSelected() {
        isSameTimes = !isSameTimes;
        handleTimeOfCareChanged();
    }

    @Override
    public void sameStartDateClicked() {
        showTimePickerDialog(getString(R.string.start_times), binding.startDateTV, 0, null, null, true);
    }

    @Override
    public void sameEndDatClicked() {
        if (sameDayModel.getStartHour() != null)
            showTimePickerDialog(getString(R.string.start_times), binding.endDateTV, 0, sameDayModel.getStartHour(), sameDayModel.getStartMinute(), false);
        else {
            DialogFactory.createErrorDialog(this, "", getString(R.string.error_select_start_time));
        }
    }

    @Override
    public void dayStartDateClicked(int index) {
        if (isSameTimes)
            return;
        showTimePickerDialog(getString(R.string.start_times), getTextViewByTag("start_" + index), index, null, null, true);

    }

    @Override
    public void dayEndDateClicked(int index) {
        if (isSameTimes)
            return;
        if (getSelectedDayByIndex(index).getStartHour() != null)
            showTimePickerDialog(getString(R.string.start_times), getTextViewByTag("end_" + index), index, getSelectedDayByIndex(index).getStartHour(), getSelectedDayByIndex(index).getStartMinute(), false);
        else {
            DialogFactory.createErrorDialog(this, "", getString(R.string.error_select_start_time) + " " + WeCareUtils.getDayNameByIndex(this, index));

        }
    }

    @Override
    public void scheduleCreatedSuccessfully() {
        DialogFactory.createFeedBackDialog(this, "", getString(R.string.success_service_schedule), getString(R.string.ok), getResources().getDrawable(R.drawable.success_img), new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog dialog, DialogAction which) {

            }
        });
    }

    @Override
    public void userScheduleFetchedSuccessfully(UserScheduleResponse userScheduleResponse) {
        if ((userScheduleResponse.getIsAvailable24() + "").equals(AppConstants.PHP_TRUE)) {
            handle24AvailabilityUI(true);
            binding.fullTimeSwitch.setChecked(true);
            return;
        }

        selectedDays = userScheduleResponse.getDayModels();
        isSameTimes = userScheduleResponse.getSameTime() != null ? userScheduleResponse.getSameTime() : false;
        if (isSameTimes) {
            String startTime = userScheduleResponse.getDayModels().get(0).getStartTime();
            String endTime = userScheduleResponse.getDayModels().get(0).getEndTime();
            sameDayModel.setStartTime(startTime);
            sameDayModel.setEndTime(endTime);
            binding.startDateTV.setText(DateUtils.updateEndTime(Integer.parseInt(startTime.split(":")[0]), Integer.parseInt(startTime.split(":")[1])));
            binding.endDateTV.setText(DateUtils.updateEndTime(Integer.parseInt(endTime.split(":")[0]), Integer.parseInt(endTime.split(":")[1])));
        }
        for (ScheduleDayModel model : selectedDays) {
            daySelected(model.getCurrentDayNumber(), true, false);
        }
        handleTimeOfCareChanged();
    }

    private void showTimePickerDialog(String title, TextView textView, int index, Integer minHour, Integer minMinute, boolean isStart) {
        DialogFactory.showTimePickerDialog(this, getSupportFragmentManager(), title, minHour, minMinute, 30, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
                textView.setText(DateUtils.updateEndTime(hourOfDay, minute));
                if (isSameTimes) {
                    if (isStart) {
                        sameDayModel.setStartTime(hourOfDay + ":" + minute);
                        sameDayModel.setStartHour(hourOfDay);
                        sameDayModel.setStartMinute(minute);
                        setAllSelectedStartDaysAsSame(sameDayModel.getStartTime() != null ? sameDayModel.getStartTime() : "");
                    } else {
                        sameDayModel.setEndTime(hourOfDay + ":" + minute);
                        setAllSelectedEndDaysAsSame(sameDayModel.getEndTime() != null ? sameDayModel.getEndTime() : "");
                    }
                } else {
                    if (isStart) {
                        getSelectedDayByIndex(index).setStartTime(hourOfDay + ":" + minute);
                        getSelectedDayByIndex(index).setStartHour(hourOfDay);
                        getSelectedDayByIndex(index).setStartMinute(minute);

                    } else
                        getSelectedDayByIndex(index).setEndTime(hourOfDay + ":" + minute);

                }
            }
        });
    }


    private void handleDayWeekButtonSelection(AppCompatButton button, LinearLayout dayView, int index, boolean isSelected) {
        button.setTextColor(getResources().getColor(isSelected ? R.color.white : R.color.colorPrimary));
        button.setBackgroundDrawable(getResources().getDrawable(isSelected ? R.drawable.rounded_dayweek_selected : R.drawable.rounded_dayweek_unselected));
        dayView.setVisibility(isSelected ? View.VISIBLE : View.GONE);

        if (!isSameTimes) {
            getTextViewByTag("start_" + index, dayView).setText(getString(R.string.select));
            getTextViewByTag("start_" + index, dayView).setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_arrow_right), null);
            getTextViewByTag("end_" + index, dayView).setText(getString(R.string.select));
            getTextViewByTag("end_" + index, dayView).setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_arrow_right), null);
        } else {

            getTextViewByTag("start_" + index, dayView).setText(binding.startDateTV.getText());
            getTextViewByTag("start_" + index, dayView).setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
            getTextViewByTag("end_" + index, dayView).setText(binding.endDateTV.getText());
            getTextViewByTag("end_" + index, dayView).setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }

    }

    private ScheduleDayModel getSelectedDayByIndex(int index) {
        for (ScheduleDayModel dayModel : selectedDays) {
            if (dayModel.getCurrentDayNumber() == index)
                return dayModel;
        }
        return null;
    }

    private void handleTimeOfCareChanged() {
        if (isSameTimes) {
            binding.timesTV.setText(getString(R.string.different_times));
            binding.sameTimeLayout.setVisibility(View.VISIBLE);
            if (selectedDays.size() == 0)
                return;
            setAllSelectedStartDaysAsSame(sameDayModel.getStartTime() != null ? sameDayModel.getStartTime() : "");
            setAllSelectedEndDaysAsSame(sameDayModel.getEndTime() != null ? sameDayModel.getEndTime() : "");
        } else {
            binding.timesTV.setText(getString(R.string.same_times));
            binding.sameTimeLayout.setVisibility(View.GONE);

            for (ScheduleDayModel dayModel : selectedDays) {
                TextView start = getTextViewByTag("start_" + dayModel.getCurrentDayNumber());
                start.setText(dayModel.getStartTime() != null ? DateUtils.updateEndTime(Integer.parseInt(dayModel.getStartTime().split(":")[0]), Integer.parseInt(dayModel.getStartTime().split(":")[1])) : getString(R.string.select));
                start.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_arrow_right), null);

                TextView end = getTextViewByTag("end_" + dayModel.getCurrentDayNumber());
                end.setText(dayModel.getEndTime() != null ? DateUtils.updateEndTime(Integer.parseInt(dayModel.getEndTime().split(":")[0]), Integer.parseInt(dayModel.getEndTime().split(":")[1])) : getString(R.string.select));
                end.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.ic_arrow_right), null);
            }
        }
    }

    private void setAllSelectedStartDaysAsSame(String startTime) {

        for (ScheduleDayModel dayModel : selectedDays) {
            dayModel.setStartTime(startTime);
            //update UI
            TextView start = getTextViewByTag("start_" + dayModel.getCurrentDayNumber());
            start.setText(binding.startDateTV.getText());
            start.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

        }
    }

    private void setAllSelectedEndDaysAsSame(String endTime) {

        for (ScheduleDayModel dayModel : selectedDays) {
            dayModel.setEndTime(endTime);
            //update UI
            TextView end = getTextViewByTag("end_" + dayModel.getCurrentDayNumber());
            end.setText(binding.endDateTV.getText());
            end.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
        }
    }


    private TextView getTextViewByTag(String tag) {
        return binding.bottomLayout.findViewWithTag(tag);
    }

    private TextView getTextViewByTag(String tag, LinearLayout layout) {
        return layout.findViewWithTag(tag);
    }

}
