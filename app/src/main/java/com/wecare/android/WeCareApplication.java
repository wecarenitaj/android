package com.wecare.android;

import android.app.Application;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.gsonparserfactory.GsonParserFactory;
import com.androidnetworking.interceptors.HttpLoggingInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.twitter.sdk.android.core.Twitter;
import com.wecare.android.data.local.prefs.AppPreferencesHelper;
import com.wecare.android.di.component.AppComponent;
import com.wecare.android.di.component.DaggerAppComponent;
import com.wecare.android.utils.AppLogger;
import com.wecare.android.utils.ResponseTypeAdapterFactory;

import java.lang.reflect.Modifier;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasAndroidInjector;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;


public class WeCareApplication extends DaggerApplication {

    @Inject
    CalligraphyConfig mCalligraphyConfig;

    @Inject
    AppPreferencesHelper appPreferencesHelper;

    private static WeCareApplication weCareApplication;

    public static synchronized WeCareApplication getInstance() {
        return weCareApplication;
    }

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        weCareApplication = this;

//        DaggerAppComponent.builder()
//                .application(this)
//                .build()
//                .inject(this);

        AppLogger.init();

        if (BuildConfig.isSeekerFlavor)
            Twitter.initialize(this);

        AndroidNetworking.initialize(getApplicationContext());
        if (BuildConfig.DEBUG) {
            AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY);
        }
        GsonBuilder builder = new GsonBuilder().registerTypeAdapterFactory(new ResponseTypeAdapterFactory());
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
//        builder.excludeFieldsWithoutExposeAnnotation();
        builder.serializeNulls();
        builder.setLenient();
        Gson gson = builder.create();

        AndroidNetworking.setParserFactory(new GsonParserFactory(gson));

        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(mCalligraphyConfig))
                .build());
//        CalligraphyConfig.initDefault(mCalligraphyConfig);
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        appComponent = DaggerAppComponent.factory().create(this, this);
        return appComponent;
    }

    public AppPreferencesHelper getAppPreferencesHelper() {
        return appPreferencesHelper;
    }
}
