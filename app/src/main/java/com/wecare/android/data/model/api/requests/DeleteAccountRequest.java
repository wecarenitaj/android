package com.wecare.android.data.model.api.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteAccountRequest {

    @SerializedName("delete_reason")
    @Expose
    private String deleteReason;
    @SerializedName("other_delete_reason")
    @Expose
    private String otherDeleteReason;

    public DeleteAccountRequest(String deleteReason, String otherDeleteReason) {
        this.deleteReason = deleteReason;
        this.otherDeleteReason = otherDeleteReason;
    }

    public String getDeleteReason() {
        return deleteReason;
    }

    public void setDeleteReason(String deleteReason) {
        this.deleteReason = deleteReason;
    }

    public String getOtherDeleteReason() {
        return otherDeleteReason;
    }

    public void setOtherDeleteReason(String otherDeleteReason) {
        this.otherDeleteReason = otherDeleteReason;
    }
}
