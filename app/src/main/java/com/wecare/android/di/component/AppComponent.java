package com.wecare.android.di.component;

import android.app.Application;
import android.content.Context;

import com.wecare.android.WeCareApplication;
import com.wecare.android.di.builder.ActivityBuilder;
import com.wecare.android.di.builder.AppActivityBuilder;
import com.wecare.android.di.builder.AppFragmentBuilder;
import com.wecare.android.di.module.AppModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component(modules = {AppModule.class, AppActivityBuilder.class, AppFragmentBuilder.class})
public interface AppComponent extends AndroidInjector<WeCareApplication> {

    void inject(WeCareApplication app);

    @Component.Factory
    interface Factory {
        AppComponent create(@BindsInstance Application app, @BindsInstance Context context);
    }

}
