package com.wecare.android.ui.base;

import static com.wecare.android.utils.maps.utils.HmsGmsUtilKt.isOnlyHms;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.databinding.ViewDataBinding;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.huawei.hms.maps.HuaweiMap;
import com.huawei.hms.maps.HuaweiMapOptions;
import com.wecare.android.R;
import com.wecare.android.utils.AppConstants;
import com.wecare.android.utils.DialogFactory;
import com.wecare.android.utils.MyLocationManager;
import com.wecare.android.utils.maps.utils.GoogleLocationManager;
import com.wecare.android.utils.maps.utils.HuaweiLocationManager;

abstract public class BaseMapActivity<T extends ViewDataBinding, V extends BaseViewModel> extends BaseActivity<T, V> implements OnMapReadyCallback, com.huawei.hms.maps.OnMapReadyCallback {

    private static final int MY_LOCATION_REQUEST_CODE = 1;

    public double defaultLongitude = 0f;
    public double defaultLatitude = 0f;

    private GoogleLocationManager locationManagerGoogle;
    private HuaweiLocationManager locationManagerHuawei;

    private GoogleMap mMap;
    private HuaweiMap huaweiMap;

    private GoogleMap.OnCameraIdleListener googleOnCameraIdleListener;
    private HuaweiMap.OnCameraIdleListener huaweiOnCameraIdleListener;


    private com.huawei.hms.maps.model.Marker hMarker;
    private Marker gMarker;
    public FrameLayout mapGroup;

    private MyLocationManager myLocationManager;
    private final String[] PERMISSIONS = new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        myLocationManager = new MyLocationManager(this);
    }

    public void mapInit() {
        if (!hasLocationPermission()) requestPermission();
        if (isOnlyHms(this)) {
            locationManagerHuawei = new HuaweiLocationManager(this);
            com.huawei.hms.maps.MapsInitializer.initialize(this);
            createAsH();
        } else {
            locationManagerGoogle = new GoogleLocationManager(this);
            createAsG();
        }
    }

    private void createAsH() {
        View child = getLayoutInflater().inflate(R.layout.hmap, mapGroup, false);
        mapGroup.addView(child);
        com.huawei.hms.maps.SupportMapFragment mapFragment = (com.huawei.hms.maps.SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.huawei_map_dynamic);
        if (mapFragment == null) {
            /*HuaweiMapOptions options = new HuaweiMapOptions();
            options.mapType(HuaweiMap.MAP_TYPE_NORMAL);
            options.compassEnabled(true);
            options.zoomGesturesEnabled(true);
            options.scrollGesturesEnabled(true);
            options.rotateGesturesEnabled(true);
            options.tiltGesturesEnabled(true);
            mapFragment = com.huawei.hms.maps.SupportMapFragment.newInstance(options);*/
            mapFragment = new com.huawei.hms.maps.SupportMapFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.huawei_map_dynamic, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }

    @SuppressLint("MissingPermission")
    public void createAsG() {
        View child = getLayoutInflater().inflate(R.layout.gmap, mapGroup, false);
        mapGroup.addView(child);
        // SupportMapFragment mapFragment = (SupportMapFragment) requireActivity().getSupportFragmentManager().findFragmentById(R.id.google_map_dynamic);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.google_map_dynamic);
        if (mapFragment == null) {
            mapFragment = new SupportMapFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.google_map_dynamic, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        Log.e("GMS", "onMapReady");
        mMap = googleMap;
        if (isGPSOpen(this) && hasLocationPermission()) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.getUiSettings().setAllGesturesEnabled(isMapAllGesturesEnabled());
            locationAnimation();
            mMap.setOnCameraIdleListener(googleOnCameraIdleListener);
        } else {
            mMap.setMyLocationEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            handleGoogleLocation();
        }

        mMap.setOnMyLocationButtonClickListener(onMyGoogleLocationButtonClickListener);
        mMap.setOnMyLocationClickListener(onMyGoogleLocationClickListener);
        if (false) {
            googleMap.setOnMapClickListener(latLng -> {
                if (gMarker != null) gMarker.remove();
                gMarker = mMap.addMarker(new MarkerOptions().position(latLng));
            });
            googleMap.setOnMapLongClickListener(latLng -> {
                if (gMarker != null) gMarker.remove();
                gMarker = mMap.addMarker(new MarkerOptions().position(latLng));
            });
        }
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(defaultLatitude, defaultLongitude), 14.0f));
        requestLocationPermission();
    }

    @Override
    public void onMapReady(HuaweiMap hMap) {
        Log.e("HMS", "onMapReady");
        huaweiMap = hMap;
        if (isGPSOpen(this) && hasLocationPermission()) {
            huaweiMap.setMyLocationEnabled(true);
            huaweiMap.getUiSettings().setMyLocationButtonEnabled(true);
            huaweiMap.getUiSettings().setAllGesturesEnabled(isMapAllGesturesEnabled());
            locationAnimation();
            huaweiMap.setOnCameraIdleListener(huaweiOnCameraIdleListener);
//            clusterManageAfterMapReady();
        } else {
            huaweiMap.setMyLocationEnabled(false);
            huaweiMap.getUiSettings().setMyLocationButtonEnabled(false);
            handleHuaweiLocation();
//            clusterManageAfterMapReady();
        }

        huaweiMap.setOnMyLocationButtonClickListener(onMyHuaweiLocationButtonClickListener);
        huaweiMap.setOnMyLocationClickListener(onMyHuaweiLocationClickListener);

        if (false) {
            huaweiMap.setOnMapClickListener(latLng -> {
                if (hMarker != null) hMarker.remove();
                hMarker = huaweiMap.addMarker(new com.huawei.hms.maps.model.MarkerOptions().position(latLng));
            });
            huaweiMap.setOnMapLongClickListener(latLng -> {
                if (hMarker != null) hMarker.remove();
                hMarker = huaweiMap.addMarker(new com.huawei.hms.maps.model.MarkerOptions().position(latLng));
            });
        }
        huaweiMap.moveCamera(com.huawei.hms.maps.CameraUpdateFactory.newLatLngZoom(new com.huawei.hms.maps.model.LatLng(defaultLatitude, defaultLongitude), 14.0f));
        requestLocationPermission();
    }

    private void addMarkerToLocationHuawei(double lat, double lng) {
        com.huawei.hms.maps.model.LatLng currentLngLat = new com.huawei.hms.maps.model.LatLng(lat, lng);
        if (hMarker != null) {
            hMarker.setPosition(currentLngLat);
        } else {
            hMarker = huaweiMap.addMarker(new com.huawei.hms.maps.model.MarkerOptions().position(currentLngLat).title("my location").draggable(true).visible(true));
//                    .icon(bitmapDescriptorFromVectorJava(requireActivity(), R.drawable.map_location_pin_svg)));
        }
        huaweiMap.animateCamera(com.huawei.hms.maps.CameraUpdateFactory.newLatLngZoom(new com.huawei.hms.maps.model.LatLng(currentLngLat.latitude, currentLngLat.longitude), 17.0f), new HuaweiMap.CancelableCallback() {
            @Override
            public void onFinish() {
                //getPositionDetail("${currentLngLat.latitude},${currentLngLat.longitude}")
            }

            @Override
            public void onCancel() {
                //getPositionDetail("${currentLngLat.latitude},${currentLngLat.longitude}")
            }
        });
    }

    private final GoogleMap.OnMyLocationButtonClickListener onMyGoogleLocationButtonClickListener = () -> {
//            mMap.setMinZoomPreference(15);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            DialogFactory.createReactDialog(this, getString(R.string.location), getString(R.string.gps_access_denied), getString(R.string.location_settings), getString(R.string.cancel), null, (dialog, which) -> {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, AppConstants.REQUEST_LOCATION_SETTING_RESULT);
            }, (dialog, which) -> onMapLocationButtonClicked());
        } else {
            onMapLocationButtonClicked();
        }

        return false;
    };

    private final GoogleMap.OnMyLocationClickListener onMyGoogleLocationClickListener = new GoogleMap.OnMyLocationClickListener() {
        @Override
        public void onMyLocationClick(@NonNull Location location) {

            mMap.setMinZoomPreference(12);

            CircleOptions circleOptions = new CircleOptions();
            circleOptions.center(new LatLng(location.getLatitude(), location.getLongitude()));

            circleOptions.radius(200);
            circleOptions.fillColor(Color.RED);
            circleOptions.strokeWidth(6);

            mMap.addCircle(circleOptions);
        }
    };

    private final HuaweiMap.OnMyLocationButtonClickListener onMyHuaweiLocationButtonClickListener = () -> {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            DialogFactory.createReactDialog(this, getString(R.string.location), getString(R.string.gps_access_denied), getString(R.string.location_settings), getString(R.string.cancel), null, (dialog, which) -> {
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(myIntent, AppConstants.REQUEST_LOCATION_SETTING_RESULT);
            }, (dialog, which) -> onMapLocationButtonClicked());
        } else {
            onMapLocationButtonClicked();
        }

        return false;
    };

    private final HuaweiMap.OnMyLocationClickListener onMyHuaweiLocationClickListener = new HuaweiMap.OnMyLocationClickListener() {
        @Override
        public void onMyLocationClick(@NonNull Location location) {

            huaweiMap.setMinZoomPreference(12);

            com.huawei.hms.maps.model.CircleOptions circleOptions = new com.huawei.hms.maps.model.CircleOptions();
            circleOptions.center(new com.huawei.hms.maps.model.LatLng(location.getLatitude(), location.getLongitude()));

            circleOptions.radius(200);
            circleOptions.fillColor(Color.RED);
            circleOptions.strokeWidth(6);

            huaweiMap.addCircle(circleOptions);
        }
    };

    /***
     * Location Methods
     * **/

    private boolean isGPSOpen(final Context context) {
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return gps || network;
    }

    private boolean hasLocationPermission() {
        for (String permission : PERMISSIONS) {
            if (ActivityCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }

    private void requestPermission() {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            Log.i("TAG", "sdk < 28 Q");
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                String[] strings = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
                ActivityCompat.requestPermissions(this, strings, 1);
            }
        } else {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, "android.permission.ACCESS_BACKGROUND_LOCATION") != PackageManager.PERMISSION_GRANTED) {
                String[] strings = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, "android.permission.ACCESS_BACKGROUND_LOCATION"};
                ActivityCompat.requestPermissions(this, strings, 1);
            }
        }
    }

    public void locationAnimation() {
        if (defaultLongitude != 0f && defaultLatitude != 0f) {
            setDefaultMapCameraLocation();
        } else {
            if (isOnlyHms(this)) {
                locationManagerHuawei.getLastKnownLocation(location -> {
                    if (location == null) {
                        Log.e("HMS", "LocationKit -> Last Known Location is empty");
                    } else {
                        huaweiMap.moveCamera(com.huawei.hms.maps.CameraUpdateFactory.newLatLng(new com.huawei.hms.maps.model.LatLng(location.getLatitude(), location.getLongitude())));
                        huaweiMap.animateCamera(com.huawei.hms.maps.CameraUpdateFactory.zoomTo(18f), 2000, null);
                        Log.e("HMS", "LocationKit -> Last Known Location: " + location.getLatitude() + ">>" + location.getLongitude() + "");
//                    addMarkerToLocationHuawei(location.getLatitude(), location.getLongitude());
                        getLocationEvent(location.getLongitude(), location.getLatitude());
                    }
                    return null;
                }, exception -> {
                    Log.e("HMS", "LocationKit -> Failed to get Last Known Location with exception: " + exception.getLocalizedMessage());
                    return null;
                });
            } else {
                locationManagerGoogle.getLastKnownLocation(location -> {
                    if (location == null) {
                        Log.e("GMS", "LocationKit -> Last Known Location is empty");
                    } else {
                        LatLng currentLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                        Log.e("GMS", "LocationKit -> Last Known Location: " + currentLatLng);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(18f), 2000, null);
                        getLocationEvent(location.getLongitude(), location.getLatitude());
                    }
                    return null;
                }, exception -> {
                    Log.e("GMS", "LocationKit -> Failed to get Last Known Location with exception: " + exception.getLocalizedMessage());
                    return null;
                });
            }
        }
    }

    private void handleGoogleLocation() {
        if (defaultLongitude != 0f && defaultLatitude != 0f) {
            setDefaultMapCameraLocation();
        } else {
            locationManagerGoogle.checkLocationSettingsAndShowPopup(this, locationSettingsResponse -> {
                locationManagerGoogle.getLastKnownLocation(location -> {
                    if (location == null) {
                        locationManagerGoogle.registerLocationUpdates(10000, location1 -> {
                            printLocation(location1);
                            return null;
                        }, locationAvailability -> null);
                    } else {
                        locationAnimation();
                        printLocation(location);
                    }
                    return null;
                }, null);
                return null;
            });
        }
    }

    private void handleHuaweiLocation() {
        if (defaultLongitude != 0f && defaultLatitude != 0f) {
            setDefaultMapCameraLocation();
        } else {
            locationManagerHuawei.checkLocationSettingsAndShowPopup(this, locationSettingsResponse -> {
                locationManagerHuawei.getLastKnownLocation(location -> {
                    if (location == null) {
                        locationManagerHuawei.registerLocationUpdates(10000, location1 -> {
                            printLocation(location1);
                            return null;
                        }, locationAvailability -> null);
                    } else {
                        locationAnimation();
                        printLocation(location);
                    }
                    return null;
                }, null);
                return null;
            });
        }
    }

    private void printLocation(Location location) {
        Log.e("LOCATION:", "Fetched Location: latitude: " + location.getLatitude() + " | longitude: " + location.getLongitude());
    }

    private void setCurrentLocationListener() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        if (isOnlyHms(this)) {
            Log.e("HMS", "onActivityResult is called");
            huaweiMap.setMyLocationEnabled(true);
            handleHuaweiLocation();
        } else {
            mMap.setMyLocationEnabled(true);
            Log.e("GMS", "onActivityResult is called");
            handleGoogleLocation();
        }
    }

    private void requestLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setCurrentLocationListener();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
/*
            // Show rationale and request permission.
            new AlertDialog.Builder(this)
                    .setTitle("mosa training")
                    .setMessage("permissions_disclosure_fine_location")
                    .setPositiveButton(getString(R.string.ok), (dialog, whichButton) -> dialog.dismiss())
                    .setNegativeButton(getString(R.string.cancel), (dialog, whichButton) -> dialog.dismiss())
                    .show();
*/
        }
    }

    private void onMapLocationButtonClicked() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (defaultLatitude != 0f && defaultLongitude != 0f) {
                moveMapCamera(defaultLongitude, defaultLatitude);
                setLocationFromCameraIdle(defaultLongitude, defaultLatitude, true);
            } else {
                getMyCurrentLocation();
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }
    }

    public void getMyCurrentLocation() {
//        if (defaultLatitude != 0f && defaultLongitude != 0f) {
        //.get location and optional request offer
        myLocationManager.getCurrentLocation((MyLocationManager.MyLocationListener) location -> {
            if (location != null && (mMap != null || huaweiMap != null)) {
                Log.d("My Location is", location.getLatitude() + " " + location.getLongitude());
                moveMapCamera(location.getLongitude(), location.getLatitude());
                setLocationFromCameraIdle(location.getLongitude(), location.getLatitude(), false);
            }
        });
//        }
    }

    public void configureCameraIdle() {
        if (isOnlyHms(this)) {
            huaweiOnCameraIdleListener = () -> {
                com.huawei.hms.maps.model.LatLng latLng = huaweiMap.getCameraPosition().target;
                Log.e("configureCameraIdle", "location : " + latLng.longitude + ", " + latLng.latitude);
                setLocationFromCameraIdle(latLng.longitude, latLng.latitude, false);
            };
        } else {
            googleOnCameraIdleListener = () -> {
                LatLng latLng = mMap.getCameraPosition().target;
                Log.e("configureCameraIdle", "location : " + latLng.longitude + ", " + latLng.latitude);
                setLocationFromCameraIdle(latLng.longitude, latLng.latitude, false);
            };
        }
    }

    private void moveMapCamera(double longitude, double latitude) {
        if (isOnlyHms(this)) {
            huaweiMap.moveCamera(com.huawei.hms.maps.CameraUpdateFactory.newLatLngZoom(new com.huawei.hms.maps.model.LatLng(latitude, longitude), 14.0f));
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 14.0f));
        }

    }

    private void setDefaultMapCameraLocation() {
        Log.e("locationAnimation", "default location -> " + defaultLatitude + ">>" + defaultLongitude);
        if (isOnlyHms(this)) {
            huaweiMap.moveCamera(com.huawei.hms.maps.CameraUpdateFactory.newLatLng(new com.huawei.hms.maps.model.LatLng(defaultLatitude, defaultLongitude)));
            huaweiMap.animateCamera(com.huawei.hms.maps.CameraUpdateFactory.zoomTo(18f), 2000, null);

        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(defaultLatitude, defaultLongitude)));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(18f), 2000, null);
        }
    }

    public abstract void setLocationFromCameraIdle(double longitude, double latitude, boolean fromOnclick);

    public abstract boolean isMapAllGesturesEnabled();

    public void getLocationEvent(double longitude, double latitude) {
    }

    /****
     * android overriding
     * ****/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.e("HMS", "request code onRequestPermissionsResult ->" + requestCode + "");
        if (requestCode == MY_LOCATION_REQUEST_CODE) {
            if (permissions.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                setCurrentLocationListener();
            } else {
                // Permission was denied. Display an error message.
                Toast.makeText(this, getResources().getString(R.string.ask_permssion), Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == HuaweiLocationManager.REQUEST_CHECK_SETTINGS) {
            switch (resultCode) {
                case Activity.RESULT_OK:
                    if (isOnlyHms(this)) {
                        handleHuaweiLocation();
                    } else {
                        handleGoogleLocation();
                    }
                    break;
                case Activity.RESULT_CANCELED:
                    Log.d("TAG", "User denied to access location");
                    break;
            }
        }
    }

    public void setMapMarkerForPosition(double longitude, double latitude) {
        if (isOnlyHms(this)) {
            com.huawei.hms.maps.model.MarkerOptions markerOptions = new com.huawei.hms.maps.model.MarkerOptions();
            com.huawei.hms.maps.model.LatLng latLng = new com.huawei.hms.maps.model.LatLng(latitude, longitude);
            markerOptions.position(latLng);
            markerOptions.title(latLng.latitude + " : " + latLng.longitude);
            huaweiMap.clear();
            huaweiMap.animateCamera(com.huawei.hms.maps.CameraUpdateFactory.newLatLng(latLng));
            moveMapCamera(longitude, latitude);
            // Placing a marker on the touched position
            huaweiMap.addMarker(markerOptions);
        } else {
            // Creating a marker
            MarkerOptions markerOptions = new MarkerOptions();
            LatLng latLng = new LatLng(latitude, longitude);
            // Setting the position for the marker
            markerOptions.position(latLng);
            // Setting the title for the marker.
            // This will be displayed on taping the marker
            markerOptions.title(latLng.latitude + " : " + latLng.longitude);
            // Clears the previously touched position
            mMap.clear();
            mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
            moveMapCamera(longitude, latitude);
            // Placing a marker on the touched position
            mMap.addMarker(markerOptions);
        }
    }

    /*
    * private void enableMapLocationButton() {
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (mapFragment != null && mMap != null) {
                mMap.setMyLocationEnabled(true);

                View locationButton = ((View) mapFragment.getView().findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
                RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();

//                // position on top right
//                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
//                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
//                rlp.setMargins(0, 180, 180, 0);

                // position on right bottom
                rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
                rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
                rlp.setMargins(0, 0, 30, 600);

                if (currentLocationModel == null) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            locationButton.performClick();
                        }
                    }, 1000);
                }
            }
        } else {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, AppConstants.REQUEST_LOCATION_PERMISSION);
        }
    }*/
}
