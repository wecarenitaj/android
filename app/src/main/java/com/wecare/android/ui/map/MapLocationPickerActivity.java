package com.wecare.android.ui.map;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.wecare.android.R;
import com.wecare.android.data.model.api.models.LocationModel;
import com.wecare.android.databinding.ActivityMapLocationPickerBinding;
import com.wecare.android.ui.base.BaseMapActivity;
import com.wecare.android.ui.base.BaseViewModel;
import com.wecare.android.utils.AppConstants;

import androidx.annotation.NonNull;

public class MapLocationPickerActivity extends BaseMapActivity<ActivityMapLocationPickerBinding, BaseViewModel> implements OnMapReadyCallback {
    private TextView resutText;
    private Button doneBtn;
    LocationModel locationModel;
    LocationModel currentLocationModel;

    @Override
    public BaseViewModel getViewModel() {
        return null;
    }

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_map_location_picker;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setStatusBarTranslucent(true);

        Intent intent = this.getIntent();

        if (intent.getExtras() != null && intent.getExtras().containsKey(AppConstants.ARGS_KEY_LOCATION_OBJECT)) {
            currentLocationModel = intent.getExtras().getParcelable(AppConstants.ARGS_KEY_LOCATION_OBJECT);
            if (currentLocationModel.getLongitude() != null){
                defaultLongitude = currentLocationModel.getLongitude();
            }
            if (currentLocationModel.getLatitude() != null){
                defaultLatitude = currentLocationModel.getLatitude();
            }
        }
        locationModel = new LocationModel();

        super.onCreate(savedInstanceState);
        // init map
        mapGroup = getViewDataBinding().map;
        mapInit();
        configureCameraIdle();

        resutText = getViewDataBinding().draggResult;
        doneBtn = getViewDataBinding().doneButton;
        doneBtn.setOnClickListener(view -> {
            locationModel.setType(AppConstants.SPECIFIC_LOCATION_LOCATION);
            Intent returnIntent = new Intent();
            returnIntent.putExtra(AppConstants.ARGS_KEY_LOCATION_OBJECT, locationModel);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        });
    }

    @Override
    public void setLocationFromCameraIdle(double longitude, double latitude, boolean fromOnclick) {
        if (fromOnclick) {
            locationModel.setLongitude(currentLocationModel.getLongitude());
            locationModel.setLatitude(currentLocationModel.getLatitude());
        } else {
            locationModel.setLongitude(longitude);
            locationModel.setLatitude(latitude);
        }
//                Geocoder geocoder = new Geocoder(MapLocationPickerActivity.this);

//                try {
//                    List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
//                    if (addressList != null && addressList.size() > 0) {
//                        String locality = addressList.get(0).getAddressLine(0);
//                        String country = addressList.get(0).getCountryName();
//                        if (locality != null && country != null) {
//                            resutText.setText(String.format("%s  %s", locality, country));
//                            locationModel.setName(String.format("%s  %s", locality, country));
//                        }
//                        locationModel.setLongitude(addressList.get(0).getLongitude());
//                        locationModel.setLatitude(addressList.get(0).getLatitude());
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
    }

    @Override
    public boolean isMapAllGesturesEnabled() {
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
       /* else if (requestCode == AppConstants.REQUEST_LOCATION_PERMISSION_BUTTON) {
            onMapLocationButtonClicked();
        } else if (requestCode == AppConstants.REQUEST_LOCATION_SETTING_RESULT) {
            onMapLocationButtonClicked();
        }*/
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }
}
