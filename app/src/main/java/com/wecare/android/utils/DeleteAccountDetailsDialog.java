package com.wecare.android.utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;

import com.wecare.android.R;

public class DeleteAccountDetailsDialog extends Dialog {
    private final DeleteAccountDetailsInterface deleteAccountInterface;

    RadioGroup radioGroup;
    EditText editText;

    public interface DeleteAccountDetailsInterface {
        void onDeleteClick(String deleteReason, String otherDeleteReason);
    }

    public DeleteAccountDetailsDialog(@NonNull Context context, DeleteAccountDetailsInterface deleteAccountInterface) {
        super(context);
        this.deleteAccountInterface = deleteAccountInterface;
    }

    boolean selectOther = false;
    boolean selectOption = false;

    String deleteReason = "";
    String otherDeleteReason = "";

    @SuppressLint({"NonConstantResourceId", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.delete_account_details_dialog);
        initUi();
    }

    private void initUi() {
        findViewById(R.id.delete_dialog_cancelButton).setOnClickListener(view -> dismiss());
        findViewById(R.id.delete_dialog_deleteButton).setOnClickListener(view -> {
            if (selectOption) {
                if (selectOther) {
                    otherDeleteReason = editText.getText().toString();
                }
                dismiss();
                deleteAccountInterface.onDeleteClick(deleteReason, otherDeleteReason);
            }
        });

        editText = findViewById(R.id.otherReasonId);
        radioGroup = findViewById(R.id.radioGroup);

        initListener();
    }


    private void initListener() {
        radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            switch ((checkedId)) {
                case R.id.no_need -> {
                    selectOption = true;
                    editText.setVisibility(View.GONE);
                    deleteReason = "No need to keep use the application";
                }
                case R.id.save_space_for_storage -> {
                    selectOption = true;
                    editText.setVisibility(View.GONE);
                    deleteReason = "Save space for storage";
                }
                case R.id.rare_use_of_the_application -> {
                    editText.setVisibility(View.GONE);
                    selectOption = true;
                    deleteReason = "Rare use of the application";
                }
                case R.id.other_than_that -> {
                    selectOther = true;
                    deleteReason = "Other than that";
                    editText.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
