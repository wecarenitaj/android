package com.wecare.android.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;

import com.wecare.android.R;

public class DeleteAccountDialog extends Dialog {


    public AppCompatTextView delete, cancel;
    private final DeleteAccountInterface deleteAccountInterface;

    public interface DeleteAccountInterface {
        void onDeleteClick();
    }

    public DeleteAccountDialog(@NonNull Context context, DeleteAccountInterface deleteAccountInterface) {
        super(context);
        this.deleteAccountInterface = deleteAccountInterface;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.delete_account_dialog);
        findViewById(R.id.delete_dialog_cancelButton).setOnClickListener(view -> dismiss());
        findViewById(R.id.delete_dialog_deleteButton).setOnClickListener(view -> {
            deleteAccountInterface.onDeleteClick();
            dismiss();
        });

    }


}
