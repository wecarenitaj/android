package com.wecare.android.di.builder;

import com.wecare.android.ui.auth.forgetpassword.ForgetPasswordActivity;
import com.wecare.android.ui.auth.forgetpassword.verfication.ForgetPasswordVerificationActivity;
import com.wecare.android.ui.auth.registration.RegistrationActivity;
import com.wecare.android.ui.create_order.CreateOrderActivity;
import com.wecare.android.ui.create_order.CreateOrderActivityModule;
import com.wecare.android.ui.create_order.location.LocationActivity;
import com.wecare.android.ui.create_order.location.LocationFragmentProvider;
import com.wecare.android.ui.create_order.location.add.AddEditLocationActivity;
import com.wecare.android.ui.create_order.relative.RelativeProfileActivity;
import com.wecare.android.ui.create_order.relative.RelativeProfileActivityModule;
import com.wecare.android.ui.create_order.relative.add.AddEditRelativeProfileActivity;
import com.wecare.android.ui.intro.IntroActivity;
import com.wecare.android.ui.login.LoginActivity;
import com.wecare.android.ui.main.MainActivity;
import com.wecare.android.ui.main.MainActivityModule;
import com.wecare.android.ui.main.home.sub.SubServicesActivity;
import com.wecare.android.ui.main.home.sub.SubServicesActivityModule;
import com.wecare.android.ui.main.order.special.SpecialRequestActivity;
import com.wecare.android.ui.main.profile.userProfile.paymentmethod.PaymentMethodActivity;
import com.wecare.android.ui.main.profile.userProfile.personalInfo.EditProfileInformationActivity;
import com.wecare.android.ui.map.MapLocationPickerActivity;
import com.wecare.android.ui.profile.CaregiverShowProfileActivity;
import com.wecare.android.ui.search_giver.SearchGiverActivity;
import com.wecare.android.ui.search_giver.SearchGiverActivityModule;
import com.wecare.android.ui.search_giver.filter.FilterGiverActivity;
import com.wecare.android.ui.splash.SplashActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class AppActivityBuilder extends ActivityBuilder {
    @ContributesAndroidInjector
    abstract SplashActivity bindSplashActivity();

    @ContributesAndroidInjector
    abstract LoginActivity bindLoginActivity();

    @ContributesAndroidInjector(modules = {MainActivityModule.class})
    abstract MainActivity bindMainActivity();

    @ContributesAndroidInjector(modules = {SubServicesActivityModule.class})
    abstract SubServicesActivity bindSubServicesActivity();

    @ContributesAndroidInjector
    abstract IntroActivity bindIntroActivity();

    @ContributesAndroidInjector
    abstract ForgetPasswordActivity bindForgetPasswordActivity();

    @ContributesAndroidInjector(modules = {RelativeProfileActivityModule.class})
    abstract RelativeProfileActivity bindRelativeProfileActivity();

    @ContributesAndroidInjector
    abstract AddEditRelativeProfileActivity bindAddRelativeProfileActivity();

    @ContributesAndroidInjector
    abstract AddEditLocationActivity bindAddLocationActivity();
    @ContributesAndroidInjector
    abstract MapLocationPickerActivity bindAMapLocationPickerActivity();

    @ContributesAndroidInjector
    abstract ForgetPasswordVerificationActivity bindForgetPasswordVerificationActivity();

    @ContributesAndroidInjector
    abstract EditProfileInformationActivity bindEditProfileInformationActivity();

    @ContributesAndroidInjector
    abstract FilterGiverActivity bindFilterGiverActivity();

    @ContributesAndroidInjector
    abstract SpecialRequestActivity bindSpecialRequestActivity();

    @ContributesAndroidInjector(modules = {SearchGiverActivityModule.class})
    abstract SearchGiverActivity bindSearchSeekerActivity();

    @ContributesAndroidInjector(modules = {CreateOrderActivityModule.class})
    abstract CreateOrderActivity bindCreateOrderActivity();


    @ContributesAndroidInjector(modules = {LocationFragmentProvider.class})
    abstract LocationActivity bindLocationActivity();

    @ContributesAndroidInjector
    abstract RegistrationActivity bindForgetRegistrationActivity();

    @ContributesAndroidInjector
    abstract CaregiverShowProfileActivity bindCaregiverShowProfileActivity();

    @ContributesAndroidInjector
    abstract PaymentMethodActivity bindPaymentMethodActivity();
}
