package com.wecare.android.di.builder;

import com.wecare.android.ui.auth.registration.info.RegistrationInfoFragment;
import com.wecare.android.ui.auth.registration.verification.RegistrationVerificationCodeFragment;
import com.wecare.android.ui.create_order.done.DoneFragment;
import com.wecare.android.ui.create_order.done.DoneFragmentModule;
import com.wecare.android.ui.create_order.info.InformationFragment;
import com.wecare.android.ui.create_order.info.InformationFragmentModule;
import com.wecare.android.ui.create_order.location.LocationFragment;
import com.wecare.android.ui.create_order.location.LocationFragmentModule;
import com.wecare.android.ui.create_order.schedule.ScheduleFragment;
import com.wecare.android.ui.create_order.services.OrderServicesFragment;
import com.wecare.android.ui.create_order.services.OrderServicesFragmentModule;
import com.wecare.android.ui.intro.fragments.first.FirstIntroFragment;
import com.wecare.android.ui.intro.fragments.forth.FourthIntroFragment;
import com.wecare.android.ui.intro.fragments.second.SecondIntroFragment;
import com.wecare.android.ui.intro.fragments.third.ThirdIntroFragment;
import com.wecare.android.ui.main.guest.GuestFragment;
import com.wecare.android.ui.main.home.sub.details.SubDetailsFragment;
import com.wecare.android.ui.main.order.OrderFragment;
import com.wecare.android.ui.main.order.OrderFragmentModule;
import com.wecare.android.ui.main.order.current.CurrentFragment;
import com.wecare.android.ui.main.order.current.CurrentFragmentModule;
import com.wecare.android.ui.main.order.previous.PreviousFragment;
import com.wecare.android.ui.main.order.previous.PreviousFragmentModule;
import com.wecare.android.ui.main.profile.ProfileMainFragment;
import com.wecare.android.ui.main.profile.userProfile.UserProfileFragment;
import com.wecare.android.ui.main.profile.wallet.WalletFragment;
import com.wecare.android.ui.main.settings.SettingsFragment;
import com.wecare.android.ui.search_giver.blocked.BlockedGiverFragment;
import com.wecare.android.ui.search_giver.blocked.BlockedGiverFragmentModule;
import com.wecare.android.ui.search_giver.favourite.FavouriteGiverFragment;
import com.wecare.android.ui.search_giver.favourite.FavouriteGiverFragmentModule;
import com.wecare.android.ui.search_giver.search.SearchGiverFragment;
import com.wecare.android.ui.search_giver.search.SearchGiverFragmentModule;
import com.wecare.android.ui.search_giver.suggested.SuggestedGiverFragment;
import com.wecare.android.ui.search_giver.suggested.SuggestedGiverFragmentModule;
import com.wecare.android.ui.sub.ServicesFragment;
import com.wecare.android.ui.sub.ServicesFragmentModule;
import com.wecare.android.ui.webview.WebViewFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AppFragmentBuilder {

    /**
     * Splash Screen
     * **/

    @ContributesAndroidInjector
    abstract FirstIntroFragment bindFirstIntroFragment();

    @ContributesAndroidInjector
    abstract SecondIntroFragment bindSecondIntroFragment();

    @ContributesAndroidInjector
    abstract ThirdIntroFragment bindThirdIntroFragment();

    @ContributesAndroidInjector
    abstract FourthIntroFragment bindFourthIntroFragment();

    @ContributesAndroidInjector
    abstract RegistrationInfoFragment provideRegistrationInfoFragmentFactory();

    @ContributesAndroidInjector
    abstract RegistrationVerificationCodeFragment provideVerificationCodeFragmentFactory();

    @ContributesAndroidInjector
    abstract WebViewFragment provideWebViewFragmentFactory();

    /***
     * MainActivity
     * */

    @ContributesAndroidInjector(modules = ServicesFragmentModule.class)
    abstract ServicesFragment provideServicesFragmentFactory();

    @ContributesAndroidInjector(modules = OrderFragmentModule.class)
    abstract OrderFragment provideOrderFragmentFactory();

    @ContributesAndroidInjector(modules = PreviousFragmentModule.class)
    abstract PreviousFragment providePreviousFragmentFactory();

    @ContributesAndroidInjector(modules = CurrentFragmentModule.class)
    abstract CurrentFragment provideCurrentFragmentFactory();

    @ContributesAndroidInjector
    abstract ProfileMainFragment provideProfileFragmentFactory();

    @ContributesAndroidInjector
    abstract SettingsFragment provideSettingsFragmentFactory();

    @ContributesAndroidInjector
    abstract UserProfileFragment provideUserProfileFragmentFactory();

    @ContributesAndroidInjector
    abstract WalletFragment provideUserWalletFragmentFactory();

    @ContributesAndroidInjector
    abstract GuestFragment provideGuestFragmentFactory();

    /***
     * SubServicesActivity
     * */

    @ContributesAndroidInjector
    abstract SubDetailsFragment provideSubDetailsFragmentFactory();

    /*****
     *
     * CreateOrderActivity
     */

    @ContributesAndroidInjector
    abstract ScheduleFragment provideScheduleFragmentFactory();

    @ContributesAndroidInjector(modules = InformationFragmentModule.class)
    abstract InformationFragment provideInformationFragmentFactory();

    @ContributesAndroidInjector(modules = LocationFragmentModule.class)
    abstract LocationFragment provideLocationFragmentFactory();

    @ContributesAndroidInjector(modules = OrderServicesFragmentModule.class)
    abstract OrderServicesFragment provideOrderServicesFragmentFactory();

    @ContributesAndroidInjector(modules = DoneFragmentModule.class)
    abstract DoneFragment provideDoneFragmentFactory();

    /**
     * SearchGiverActivity
     *
     */

    @ContributesAndroidInjector(modules = SuggestedGiverFragmentModule.class)
    abstract SuggestedGiverFragment provideSuggestedGiverFragmentFactory();

    @ContributesAndroidInjector(modules = FavouriteGiverFragmentModule.class)
    abstract FavouriteGiverFragment provideFavouriteGiverFragmentFactory();

    @ContributesAndroidInjector(modules = BlockedGiverFragmentModule.class)
    abstract BlockedGiverFragment provideBlockedGiverFragmentFactory();

    @ContributesAndroidInjector(modules = SearchGiverFragmentModule.class)
    abstract SearchGiverFragment provideSearchGiverFragmentFactory();

}
