
package com.wecare.android.ui.main.order.current;

import com.wecare.android.data.model.api.models.OrderModel;
import com.wecare.android.utils.AppConstants;

import androidx.databinding.ObservableBoolean;
import androidx.databinding.ObservableField;


public class CurrentItemViewModel {

    private OrderModel orderModel;

    public ObservableField<String> orderUserImg = new ObservableField<>();
    public ObservableField<String> seekerNameTxt = new ObservableField<>();
    public ObservableField<String> requiredServiceTxt = new ObservableField<>();
    public ObservableField<String> orderDateTimeTxt = new ObservableField<>();
    public ObservableField<String> orderTotalTxt = new ObservableField<>();
    public ObservableField<String> orderStatusTxt = new ObservableField<>();

    public ObservableField<Float> getRating() {
        return rating;
    }

    public final ObservableField<Float> rating = new ObservableField<>();


    public ObservableBoolean getIsFavorite() {
        return isFavorite;
    }

    private final ObservableBoolean isFavorite = new ObservableBoolean(false);


    private CurrentItemViewModelListener mListener;

    public CurrentItemViewModel(OrderModel ordersResponse, CurrentItemViewModelListener listener) {
        this.orderModel = ordersResponse;
        this.mListener = listener;
        this.seekerNameTxt.set(orderModel.getCaregiver().getFirstName() + " " + orderModel.getCaregiver().getLastName());
        if (orderModel.getServices().size() > 0)
            this.requiredServiceTxt.set(orderModel.getServices().get(0).getSubService().getServiceName());
        this.orderDateTimeTxt.set(orderModel.getDate());
        this.orderTotalTxt.set(orderModel.getEstimatedTotalAmount());
        this.orderUserImg.set(orderModel.getCaregiver().getAvatar());
        this.orderStatusTxt.set(orderModel.getOrderStatusModel().getEnglishName());
        this.isFavorite.set(ordersResponse.getCaregiver().getIsFavorite() == AppConstants.PHP_TRUE_RAW);
        rating.set((float) orderModel.getCaregiver().getRating());
    }


    public void onItemClicked() {
        mListener.onItemClicked(orderModel);
    }

    public interface CurrentItemViewModelListener {


        void onItemClicked(OrderModel ordersResponse);
    }
}
